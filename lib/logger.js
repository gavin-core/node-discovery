const core = require('node-core')
const events = require('events')
const networking = require('node-network')
const servicePackage = require('./service-package')

const _ = core._

let _discovery
let _context = ''
const _options = {
  path: '/' + servicePackage.name + '/' + servicePackage.version,
  method: 'POST'
}
const _logServiceName = 'log-service'

let _log

const _setContext = context => {
  _context = context
}

const _setLogLevel = level => {
  core.logger.LogLevel = level
}

const _logError = (obj, context) => {
  _log(core.logger.LogLevels.error, context || _context, obj)
}

const _logWarning = (obj, context) => {
  _log(core.logger.LogLevels.warning, context || _context, obj)
}

const _logNone = (obj, context) => {
  _log(core.logger.LogLevels.none, context || _context, obj)
}

const _logDebug = (obj, context) => {
  _log(core.logger.LogLevels.debug, context || _context, obj)
}

const _logVerbose = (obj, context) => {
  _log(core.logger.LogLevels.verbose, context || _context, obj)
}

const _logInfo = (obj, context) => {
  _log(core.logger.LogLevels.info, context || _context, obj)
}

const LogServiceDependentLogger = function () {
  const _this = core.logger.__logger.call(this)
  const _offlineQueue = []

  const _logPostingFailed = (err, level, context, obj) => {
    console.log(err.red)
    _offlineQueue.push({
      level: level,
      context: context,
      obj: obj
    })
  }

  _this.setContext = _setContext
  _this.setLogLevel = _setLogLevel
  _this.error = _logError
  _this.warning = _logWarning
  _this.capture = _logNone
  _this.info = _logInfo
  _this.verbose = _logVerbose
  _this.debug = _logDebug

  _this.log = _log = (level, context, obj = {}, ignoreConsoleWriteout) => {
    if (_this.LogLevel < level) {
      return
    }

    if (typeof obj === 'string') {
      obj = { message: obj }
    }

    if (typeof obj === 'number') {
      obj = { message: obj.toString() }
    }

    if (!obj._level) {
      obj._level = level
    }

    if (!obj._date) {
      obj._date = new Date()
    }

    try {
      if (!ignoreConsoleWriteout) {
        _this.getLogLevelColor(level, (err, color) => {
          if (err) {
            console.log('Error received but not catered for')
            console.error(err)
            return
          }

          console.log((obj.message || obj)[color])
        })
      }

      if (!_discovery) {
        // _logPostingFailed('Discovery Service is not available', level, context, obj);
        return
      }

      _discovery.getService(_logServiceName, servicePackage.services[_logServiceName].version, (err, service) => {
        if (err || !service) {
          return _logPostingFailed('Log service details not available yet. Log entry cached', level, context, obj)
        }

        _options.host = _.findWhere(service.interfaces, { key: 'web' }).host
        _options.port = _.findWhere(service.interfaces, { key: 'web' }).port

        if (context) {
          obj._context = context
        }

        networking.http.request(_options, obj, (err, result) => {
          if (!err) {
            return
          }

          console.error(err)
          _logPostingFailed('Log posting failed', level, context, obj)
        })
      })
    } catch (err) {
      _logPostingFailed('Log posting exception caught', level, context, obj)
    }
  }

  let _offlineQueueInterval
  const _processOfflineQueue = () => {
    for (let i = _offlineQueue.length - 1, item; i >= 0; i--) {
      item = _offlineQueue.pop()
      _this.log(item.level, item.context, item.obj, true)
    }
  }

  _this.on('discovery-connected', discovery => {
    _discovery = discovery
    _processOfflineQueue()
    _offlineQueueInterval = setInterval(_processOfflineQueue, 10000)
  })

  _this.on('discovery-disconnected', () => {
    _discovery = null
    clearInterval(_offlineQueueInterval)
  })

  return _this
}

module.exports = (() => {
  if (!(_logServiceName in servicePackage.services)) {
    return core.logger
  }

  LogServiceDependentLogger.prototype = events.EventEmitter.prototype
  core.logger = new LogServiceDependentLogger()
  return core.logger
})()
