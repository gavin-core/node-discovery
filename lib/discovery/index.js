const events = require('events')
const core = require('node-core')
const networking = require('node-network')
const servicePackage = require('../service-package')
const cache = require('./cache')
const handlers = require('./handlers')

const addressUtils = networking.addressUtils
const config = core.config
const UdpSocket = networking.UdpSocket
const Version = core.Version
const _ = core._

let _logger

const udpDiscover = cb => {
  let _broadcastSocket
  const _options = {
    address: process.network.myAddress,
    broadcastAddress: process.network.broadcastAddress,
    port: null
  }
  const _broadcastableNetwork = !((config.broadcastableNetwork === false || config.broadcastableNetwork === 0 || config.broadcastableNetwork === 'false' || config.broadcastableNetwork === '0'))
  let _searchInterval

  const searchForDisco = () => {
    _logger.debug('Searching for Disco')

    if (_broadcastableNetwork) {
      return _broadcastSocket.send('SEARCHING_FOR_DISCO', null, { port: core.config.discovery.udpPort })
    }

    const addresses = addressUtils.getAddressesInRange()
    for (let i = 0; i < addresses.length; i++) {
      _broadcastSocket.send('SEARCHING_FOR_DISCO', null, { address: addresses[i], port: core.config.discovery.udpPort })
    }
  }

  _broadcastSocket = new UdpSocket(_options, {
    'SEARCHING_FOR_DISCO_RESP': (req, res) => {
      clearInterval(_searchInterval)
      _broadcastSocket.close()
      cb(null, req.data)
    }
  })

  _broadcastSocket.on('close', () => {
    _logger.debug('Broadcast Server Closed')
  })

  _broadcastSocket.start(() => {
    searchForDisco()
    _searchInterval = setInterval(function () {
      searchForDisco()
    }, 1000)
  })
}

const Discovery = function (logger) {
  _logger = logger

  const _this = this
  let _tcpClient
  let _serviceInfo = {}
  const _dateStarted = +new Date()

  _this.connected = false

  _this.serviceInfo = () => _.extend({}, _serviceInfo)

  _this.discover = (cb, force) => {
    cb = cb || (() => {})

    if (!force && _this.connected) {
      return cb()
    }

    if (_tcpClient) {
      return _tcpClient.close(cb)
    }

    udpDiscover((err, data) => {
      if (err) {
        console.log('Error received but not catered for')
        console.error(err)
        return
      }

      _logger.debug('discovered')
      _this.emit('discovered')

      _tcpClient = new networking.SocketClient({ host: data.tcpServer.address, port: data.tcpServer.port }, [], handlers.rpcs)
      handlers.registerEventListeners.call(_this, _tcpClient, _logger)

      _tcpClient.on('connect', function () {
        _this.connected = true

        _logger.emit('discovery-connected', _this)
        _logger.debug('connected to disco')

        cache.services.clear()
        cache.dbs.clear()
        _this.emit.callApply(_this, 'connected', arguments)
        cb()
      })

      _tcpClient.on('disconnect', () => {
        _tcpClient = null
        _this.connected = false

        _logger.emit('discovery-disconnected')
        _logger.debug('disconnected from disco')

        _this.emit('disconnected')
        _this.discover(() => {
          registration()
        })
      })

      _tcpClient.on('error', _this.emit)
    })
  }

  let registration = false
  _this.register = function (data, interfaces, cb) {
    switch (arguments.length) {
      case 3:
        break
      case 2:
        cb = interfaces
        interfaces = null
        break
      case 1:
        cb = data
        data = null
        interfaces = null
        break
      case 0:
        break
      default:
        throw new Error('Too many arguments provided, Discovery.register(data, interfaces, cb)')
    };

    const _data = data || {}
    const _interfaces = interfaces || []
    const _cb = cb || (() => {})

    if (!_this.connected) {
      return _cb('Not connected')
    }

    if (registration) {
      return _cb('Cannot register twice')
    }

    let _resultCb = function () {
      _resultCb = () => {}
      cb.callApply(cb, arguments)
    }

    _serviceInfo.type = _this.CLIENT_TYPES.SERVICE
    _serviceInfo.name = servicePackage.name
    _serviceInfo.version = new Version(servicePackage.version, '0').toString()
    _serviceInfo.serviceId = _this.serviceId
    _serviceInfo.dateStarted = _dateStarted

    _serviceInfo.interfaces = _interfaces
    if (!Array.isArray(_interfaces)) {
      _serviceInfo.interfaces = []

      for (let key in _interfaces) {
        if (_interfaces.hasOwnProperty(key)) {
          _serviceInfo.interfaces.push(_.extend({ key: key }, _interfaces[key]))
        }
      }
    }

    _serviceInfo.services = []
    for (let name in servicePackage.services) {
      if (servicePackage.services.hasOwnProperty(name)) {
        _serviceInfo.services.push({ name: name, version: servicePackage.services[name] })
      }
    }

    _serviceInfo = _.extend(_serviceInfo, _data)

    registration = () => {
      _tcpClient.rpcs.register(_serviceInfo, (err, respData) => {
        if (err) {
          return _resultCb(err)
        }

        _this.serviceId = respData.serviceId
        _serviceInfo.serviceId = _this.serviceId

        _resultCb(null, respData)
      })
    }

    registration()
  }

  _this.getServices = (type, version, cb) => {
    if (!_tcpClient) {
      return cb(new Error('Not connected to discovery'))
    }

    // if service and version in cache, use item from cache...
    const cacheServices = cache.services.get(type, version)
    if (cacheServices) {
      return cb(null, cacheServices)
    }

    _tcpClient.rpcs.getServices({ type: type, version: version }, (err, services) => {
      if (err) {
        return cb(err)
      }

      if (!services || !services.length) {
        return cb()
      }

      cache.services.set(type, services[0].version, services)
      _this.getServices(type, version, cb)
    })
  }

  _this.getService = (type, version, cb) => {
    if (!_tcpClient) {
      return cb(new Error('Not connected to discovery'))
    }

    // if service and version in cache, use item from cache...
    const cacheService = cache.services.getNext(type, version)
    if (cacheService) {
      return cb(null, cacheService)
    }

    _tcpClient.rpcs.getServices({ type: type, version: version }, (err, services) => {
      if (err) {
        return cb(err)
      }

      if (!services || !services.length) {
        return cb()
      }

      cache.services.set(type, services[0].version, services)
      _this.getService(type, version, cb)
    })
  }

  _this.getServiceInterface = (type, version, interfaceKey, cb) => {
    _this.getService(type, version, (err, service) => {
      if (err) {
        return cb(err)
      }

      if (!service) {
        return cb()
      }

      const _interface = _.findWhere(service.interfaces, { key: interfaceKey })
      if (!_interface) {
        return cb(new Error(`${type} doesn't implement a '${interfaceKey}' interface`))
      }
      return cb(null, _interface)
    })
  }

  _this.getDbServer = (name, cb) => {
    if (!_tcpClient) {
      return cb(new Error('Not connected to discovery'))
    }

    const cacheDbServer = cache.dbs.get(name)
    if (cacheDbServer) {
      return cb(null, cacheDbServer)
    }

    _tcpClient.rpcs.getDbServer({ name: name }, (err, data) => {
      if (err) {
        return cb(err)
      }

      if (!data) {
        return cb(new Error(`No such database server found: ${name}`))
      }

      cache.dbs.set(name, data)
      return _this.getDbServer(name, cb)
    })
  }

  _this.getEnvVar = (key, cb) => {
    if (!_tcpClient) {
      return cb(new Error('Not connected to discovery'))
    }

    _tcpClient.rpcs.getEnvVar({ key: key }, (err, value) => {
      if (err) {
        return cb(err)
      }

      cb(null, value)
    })
  }

  _this.CLIENT_TYPES = {
    DISCOVERY_SERVICE: 1,
    SERVICE: 2,
    SPECTATOR: 4
  }

  return _this
}

Discovery.prototype = events.EventEmitter.prototype
module.exports = function (logger) {
  module.exports = new Discovery(logger)
  return module.exports
}
