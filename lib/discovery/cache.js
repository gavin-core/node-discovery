const core = require('node-core')
const Cache = require('mem-cache')

const Version = core.Version
const _ = core._

const ServiceCacheManager = function () {
  const _this = this
  let _cache = new Cache()
  const _index = {}

  _this.get = (type, version) => {
    if (!type && !type.length) {
      throw new Error('serviceCacheManager.get(type, version) expects at least a \'type\'')
    }

    const services = _cache.get(type)
    const versions = _.keys(services)

    if (!versions || !versions.length) {
      return []
    }

    if (version && version.length) {
      version = new Version(version, '*')
    }

    if (version && version.length) {
      return services[version.maxMatch(versions)]
    }

    var maxVersion = _.sortBy(versions, (a, b) => a < b)

    return services[maxVersion]
  }

  _this.getNext = (type, version) => {
    const services = _this.get(type, version)
    if (!services || !services.length) {
      return null
    }

    const typeLC = type.toLowerCase()

    if (!(typeLC in _index)) {
      _index[typeLC] = {}
    }

    if (!(services[0].version in _index[typeLC])) {
      _index[typeLC][services[0].version] = 0
    }

    if (_index[typeLC][services[0].version] >= services.length) {
      _index[typeLC][services[0].version] = 0
    }

    const service = services[_index[typeLC][services[0].version]]
    ;_index[typeLC][services[0].version]++

    return service
  }

  _this.set = (type, version, services) => {
    if (!type || !type.length) {
      throw new Error('serviceCacheManager.set(type, version, services) expects \'type\'')
    }

    if (!version) {
      throw new Error('serviceCacheManager.set(type, version, services) expects \'version\'')
    }

    if (version.indexOf('*') >= 0) {
      throw new Error('serviceCacheManager.set(type, version, services) doesn\'t allow for a regex version (ie, 0.* ... 0.0.1 is expected)')
    }

    if (!services) {
      services = []
    }

    if (!Array.isArray(services)) {
      services = [services]
    }

    version = new Version(version, '0')

    var obj = {}
    obj[version] = services
    _cache.set(type, obj)
  }

  _this.add = (type, version, services) => {
    if (!type || !type.length) {
      throw new Error('serviceCacheManager.add(type, version, services) expects \'type\'')
    }

    if (!version || !version.length) {
      throw new Error('serviceCacheManager.add(type, version, services) expects \'version\'')
    }

    if (version.indexOf('*') >= 0) {
      throw new Error('serviceCacheManager.add(type, version, services) doesn\'t allow for a regex version (ie, 0.* ... 0.0.1 is expected)')
    }

    if (!services) {
      throw new Error('serviceCacheManager.add(type, version, services) expects a service or array of services')
    }

    if (!Array.isArray(services)) {
      services = [services]
    }

    version = new Version(version, '0')

    let cacheServices = _this.get(type, version)

    for (let i = services.length - 1; i >= 0; i--) {
      for (let j = cacheServices.length - 1; j >= 0; j--) {
        if (services[i] === cacheServices[j]) {
          services.splice(i, 1)
        }
      }
    }

    cacheServices = cacheServices.concat(services)
    _this.set(type, version, cacheServices)
  }

  _this.remove = (type, version, services) => {
    if (!type || !type.length) {
      throw new Error('serviceCacheManager.remove(type, version, services) expects \'type\'')
    }

    if (!version || !version.length) {
      throw new Error('serviceCacheManager.remove(type, version, services) expects \'version\'')
    }

    if (version.indexOf('*') >= 0) {
      throw new Error('serviceCacheManager.remove(type, version, services) doesn\'t allow for a regex version (ie, 0.* ... 0.0.1 is expected)')
    }

    if (!Array.isArray(services)) {
      services = [services]
    }

    version = new Version(version, '0')

    let cacheServices = _this.get(type, version)

    for (let i = services.length - 1; i >= 0; i--) {
      for (let j = cacheServices.length - 1; j >= 0; j--) {
        if (services[i]._id === cacheServices[j]._id) {
          cacheServices.splice(j, 1)
        }
      }
    }

    _this.set(type, version, cacheServices)
  }

  _this.clear = (type, version) => {
    if (!(type && type.length) && !(version && version.length)) {
      _cache = new Cache()
      return
    }

    if (!version || !version.length) {
      return _cache.set(type, {})
    }

    version = new Version(version, '*')

    const services = _cache.get(type)
    let versions = _.keys(services)

    versions = version.matches(versions)

    for (let i = 0; i <= versions.length; i++) {
      delete services[version[i]]
    }

    _cache.set(type, services)
  }

  return _this
}

const DbCacheManager = function () {
  const _this = this
  let _cache = new Cache()

  _this.get = id => _cache.get(id)

  _this.set = (id, dbs) => {
    _cache.set(id, dbs)
  }

  /*
  _this.add = (id, dbs) => {
    if (!Array.isArray(dbs)) {
      dbs = [dbs]
    }

    let cachedDbs = _this.get(id)

    for (let i = dbs.length - 1; i >= 0; i--){
      for (let j = cachedDbs.length - 1; j>= 0; j--) {
        if (dbs[i] == cachedDbs[j]) {
          dbs = dbs.splice(i, 1)
        }
      }
    }

    cachedDbs = cachedDbs.concat(dbs)
    _this.set(id, cachedDbs)
  }

  _this.remove = (id, dbs) => {
    if (!Array.isArray(dbs)) {
      dbs = [dbs]
    }

    let cachedDbs = _this.get(id)

    for (let i = dbs.length - 1; i >= 0; i--) {
      for (let j = cachedDbs.length - 1; j>= 0; j--) {
        if (dbs[i] == cachedDbs[j]) {
          cachedDbs = cachedDbs.splice(j, 1)
        }
      }
    }

    _this.set(id, cachedDbs)
  }
  */

  _this.clear = id => {
    if (!id || !id.length) {
      _cache = new Cache()
      return
    }

    _cache.set(id, null)
  }

  return _this
}

module.exports = {
  services: new ServiceCacheManager(),
  dbs: new DbCacheManager()
}
