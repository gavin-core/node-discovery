const core = require('node-core')
const mongodb = require('mongodb')
const mongojs = require('mongojs')
const events = require('events')
const Discovery = require('./discovery')

const logger = core.logger

/*******************************************************************************************************************************/
exports.MongoDBConnection = function (conf, _Discovery) {
  const _this = this
  const _conf = conf
  let _reconnectHandle = null
  let _uri = null

  this.key = conf.key
  this.db = null

  const _tryConnect = () => {
    if (_reconnectHandle) {
      return
    }

    _reconnectHandle = setInterval(function () {
      logger.debug('Attempting to reconnect to DB')

      if (_this.db && _this.db.serverConfig && _this.db.serverConfig.connectionPool._poolState === 'connected' && _reconnectHandle) {
        clearInterval(_reconnectHandle)
        _reconnectHandle = null
      } else {
        _connect()
      }
    }, 500)
  }

  const _connect = () => {
    console.log('mongodb')
    ;(_Discovery || Discovery).getDbServer(_this.key, (err, serverInfo) => {
      if (err) {
        console.log('Error received but not catered for')
        console.error(err)
      }

      if (_uri != null && serverInfo.uri === _uri) {
        return
      }

      _uri = serverInfo.uri

      mongodb.connect(serverInfo.uri, (err, db) => {
        if (err) {
          return
        }

        if (_reconnectHandle) {
          clearInterval(_reconnectHandle)
          _reconnectHandle = null
        }

        _this.db = db

        db.on('close', () => {
          logger.debug('Db connection closed... fetch again...')
          _tryConnect()
        })

        db.on('error', () => {
          logger.error('connection failed 1s', logger.types.error)
        })

        _this.emit('connect', _conf, db)
        if (_conf.connected) {
          _conf.connected(db)
        }
      })
    })
  }

  _connect()
}
exports.MongoDBConnection.prototype = events.EventEmitter.prototype
/*******************************************************************************************************************************/

/*******************************************************************************************************************************/
exports.MongoJSConnection = function (conf, _Discovery) {
  const _this = this
  const _conf = conf
  // let _reconnectHandle = null
  let _uri = null

  this.key = conf.key
  this.db = null

  /*
  const _tryConnect = () => {
    _reconnectHandle = setInterval(function () {
      logger('Attempting to reconnect to DB')

      if (_this.db && _this.db.serverConfig && _this.db.serverConfig.connectionPool._poolState === 'connected' && _reconnectHandle) {
        clearInterval(_reconnectHandle)
        _reconnectHandle = null
      } else {
        _connect()
      }
    }, 500)
  }
  */

  const _connect = () => {
    ;(_Discovery || Discovery).getDbServer(_this.key, (err, serverInfo) => {
      if (err) {
        _this.emit('error', err)
        return
      }

      if (_uri != null && serverInfo.uri === _uri) {
        return
      }

      _uri = serverInfo.uri

      const db = _this.db = mongojs(serverInfo.uri, _conf.collections)

      db.on('error', () => {
        logger.error('connection failed 1s', logger.types.error)
      })

      db.on('connect', () => {
        // connect only happens at first request
        // so we just act as if we connected
      })

      _this.emit('connect', _conf, db)
      if (_conf.connected) {
        _conf.connected(db)
      }
    })
  }

  _connect()
}
exports.MongoJSConnection.prototype = events.EventEmitter.prototype
/*******************************************************************************************************************************/

/*******************************************************************************************************************************/
exports.MongoConnectionPool = function (configurations, _Discovery) {
  const _this = this
  const _connections = {}
  let connectedCount = 0

  for (let i = 0; i < configurations.length; i++) {
    const conf = configurations[i]
    _connections[conf] = conf.type === 'mongodb' ? new exports.MongoDBConnection(conf, _Discovery) : new exports.MongoJSConnection(conf, _Discovery)
    _connections[conf].on('connect', (conf, db) => {
      _this[conf.key] = db
      if (++connectedCount === configurations.length) {
        _this.emit('connect', db)
      }
    })
    _connections[conf].on('error', err => {
      _this.emit('error', err)
    })
  }
}
exports.MongoConnectionPool.prototype = events.EventEmitter.prototype
/*******************************************************************************************************************************/

// --------------------------- SAMPLE USAGE ---------------------------
// var db = new MongoConnectionPool([
//    {key: 'member_settings', collections: ['entity_vars']},
//    {key: 'member_mappings', type: 'mongodb'}
// ])

// db.member_settings.entity_vars.find({}, function (err, result) {
//    console.log(result.length)
// })
// db.member_mappings.collection('entity_vars').find({}, function (err, result) {
//    result.toArray(function (err, data) {
//        console.log(data)
//    })
// })

// --------------------------- SAMPLE USAGE ---------------------------
