// const core = require('autobot-core')
const cache = require('./cache')
const servicePackage = require('../service-package')

// const Version = core.Version

module.exports = {
  rpcs: ['REGISTER', 'GET_SERVICES', 'GET_DB_SERVER', 'GET_ENV_VAR', 'PING'],
  registerEventListeners: function (socket, logger) {
    const _this = this
    const _socket = socket
    const _logger = logger

    const _handleDiscoveryServiceRegistered = info => {
      if (info.data.address !== _socket.address().address) {
        return
      }

      _logger.debug('Discovery Service On Same IP Started')
      _socket.close()
    }

    _socket.on('SAFE_KILL', data => {
      data = data.data

      if (typeof data === 'string' && data.length) {
        logger.error(data)
      } else if (data && data.msg && data.msg.length) {
        logger.error(data.msg)
      }

      process.emit('SAFE_KILL')
    })

    _socket.on('DISCOVERY_MASTER_FOUND', _handleDiscoveryServiceRegistered)
    _socket.on('DISCOVERY_SERVICE_REGISTERED', _handleDiscoveryServiceRegistered)

    _socket.on('SERVICE_REGISTERED', serviceInfo => {
      serviceInfo = serviceInfo.data

      if (serviceInfo.name in servicePackage.services /* && new Version(serviceInfo.version).matches(servicePackage.services[serviceInfo.name].version) */) {
        cache.services.add(serviceInfo.name, serviceInfo.version, serviceInfo)
      }

      _this.emit('service-registered', serviceInfo)
    })

    _socket.on('SERVICE_DEREGISTERED', serviceInfo => {
      serviceInfo = serviceInfo.data
      cache.services.remove(serviceInfo.name, serviceInfo.version, serviceInfo)
      _this.emit('service-deregistered', serviceInfo)
    })
  }
}
