const path = require('path')

const dir = require.main.filename.substring(0, require.main.filename.lastIndexOf(path.sep))
let servicePackage = null

const parts = dir.split(path.sep)
let i = parts.length
let directory = parts.join(path.sep)

while (!servicePackage && i > 0) {
  directory = path.resolve(directory, 'package.json')

  try {
    servicePackage = require(directory)
  } catch (e) {} finally {
    directory = parts.slice(0, --i).join(path.sep)
  }
}

servicePackage = servicePackage || {}
servicePackage.services = servicePackage.services || {}
delete servicePackage.dependencies

module.exports = servicePackage
