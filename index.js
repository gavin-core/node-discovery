require('./prototypes')

const servicePackage = require('./lib/service-package')
const logger = require('./lib/logger')
const Discovery = require('./lib/discovery')(logger)

module.exports = {
  Discovery: Discovery,
  logger: logger,
  mongo: require('./lib/mongodb-connection'),
  mongodb: require('mongodb'),
  mongojs: require('mongojs'),
  mssql: require('./lib/mssql'),
  servicePackage: servicePackage
}
