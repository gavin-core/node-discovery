const core = require('node-core')
const tds = require('tedious')
const ConnectionPool = require('tedious-connection-pool')
const urijs = require('uri-js')

const _ = core._

/*
const _connection = config => {
  const _this = new tds.Connection(config)

  _this.on('connect', config.onConnect || (() => {}))

  return _this
}
*/

const _request = (cmd, conn, cb) => {
  cb = cb || (() => {})

  const _result = []
  let _item
  const _this = new tds.Request(cmd, err => {
    // conn.close();
    if (err) {
      return cb(err)
    }

    return cb(null, _result)
  })

  _this.on('row', columns => {
    _item = {}
    columns.forEach(column => {
      _item[column.metadata.colName || 'value'] = column.value
    })
    _result.push(_item)
  })

  return _this
}

const pools = {}
module.exports = {
  getConnectionFromUri: (uri, config, cb) => {
    const params = urijs.parse(uri)
    if (!params || !params.path) {
      return exports.connect({})
    }

    if (!cb && typeof config === 'function') {
      cb = config
      config = null
    }

    const connConfig = {
      userName: params.userinfo.split(':')[0],
      password: params.userinfo.split(':')[1],
      server: params.host,
      options: {
        database: params.path.substring(params.path.lastIndexOf('/') + 1, params.path.length) || 'default',
        port: params.port
      },
      onConnect: cb
    }

    if (config && typeof config === 'object') {
      if (config.options && typeof config.options === 'object') {
        _.extend(connConfig.options, config.options)
        delete config.options
      }
      _.extend(connConfig, config)
    }

    module.exports.getConnection(connConfig, cb)
  },
  getConnection: (config, cb) => {
    const key = config.userName + config.password + config.server + config.options.database + config.options.port
    if (!pools[key]) {
      pools[key] = new ConnectionPool({max: 64, min: 1}, config)
      pools[key].on('error', err => {
        console.error(key)
        console.error(err)
      })
    }

    pools[key].acquire((err, conn) => {
      if (err) {
        return cb(err)
      }

      conn.query = (cmd, cb) => {
        conn.execSql(new _request(cmd, conn, cb))
      }

      conn.prepareExec = (cmd, cb) => {
        return new _request(cmd, conn, cb)
      }

      cb(null, conn)
    })
  },
  TYPES: tds.TYPES
}
