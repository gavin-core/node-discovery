'use strict'

const mongo = require('mongodb')
global.ObjectID = mongo.ObjectID

if (!('toObjectID' in String.prototype)) {
  String.prototype.toObjectID = function () {
    try {
      return mongo.ObjectID(this)
    } catch (err) {
      console.error(err)
      return null
    }
  }
}

if (!('toObjectID' in mongo.ObjectID.prototype)) {
  mongo.ObjectID.prototype.toObjectID = function () {
    return this
  }
}

if (!(('toObjectID') in Object.prototype)) {
  Object.prototype.toObjectID = function () {
    try {
      return mongo.ObjectID(this)
    } catch (err) {
      console.error(err)
      return null
    }
  }
}
